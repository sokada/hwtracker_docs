Updates
=======
version 3.2.1 2016-06-20
------------------------
- Preference name changed

UPDATE  `hwtrack`.`be_preferences` SET  `name` =  'opportunity_grades' WHERE  `be_preferences`.`name` =  'opportunity_grades' AND  `be_preferences`.`value` =  '6,7,8,9,10' LIMIT 1 ;

- Service Learning Outcomes added

    - assets/fonts glyphicons added

    - DB hw_service_opportunities added
    
    - Service Opportunites page is added to Teacher/Student/Admin

    - `service_learning_outcomes` is added to DB be_preferences and plugin/controllers/service_tracker_settings

        - If this one is YES, then Service Learning Outcomes menu point is displaed in Student and Teacher.

        - Outcomes field is displayed in Student/Teacher/Admin home and edit pages.

    - Resources `Service Learning Outcomes` is add in the backend to display the menu in Admin. DB hw_service_outcomes is added.

    - number of outcomes checked added to preference, `num_checkbox`. This limits the maximum number of checkbox is selected.

    - Preference `reflection` is added to plugin/service_tracker_settings

        - This allows Student to add or edit a reflection and dispaly in student/service homepage.


    - preference `service_goals` is added to DB be_preferences. plugin/controllers/Service_tracker_settings

        - DB hw_service_goals is added

        - DB preference goal_instruction and service_goals are added

    - Student is not able to change their parent email setting. Only admin can do.


    - Activation process added after registration and login from Google auth. This one was needed since logging in from Google auth allow without authorization process.


    - Preference `teacher_advisory_menu` is added.



version 3.1.0 2015-08-15
------------------------

- DB ci_seeions updated

For MySQL:

    CREATE TABLE IF NOT EXISTS `ci_sessions` (
        `id` varchar(40) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        PRIMARY KEY (id),
        KEY `ci_sessions_timestamp` (`timestamp`)
    );

- application/composer integrated
- Simplepie and oauth2 added to composer
- updated for PHP5.6
- DB `hw_service` updated, add `service_advisor_id` and `service_advisor_name`
- DB preference docs added for readthedocs.org link
- ~~DB `hw_homework_comments`, `hw_service_comments` added~~
- DB in `be_preferences`, `homework_comments` and `service_comments` added 
- DB preference school_color added
- Added rules to boolean and dropdown in settings
- service email will be sent to advisor
- email to service_admin from service tracker when Staff are not chosen
- Added service advisory page to service menu to show advisory group as assignment menu do
- Update Analytics module for admin dashboard
- if `service_advisor_name`, add service admin id to `service_advisor_id`
- Update student service edit page by adding Staff and non-staff
- Settings for docs added to hwtracker settings to add a link
- Menu for Parent view
- Parent can see Homework and Service.
- color picker is added to admin hwtracker settings for school_color
- google client, secret and redirect for live and localhost are added to preference


