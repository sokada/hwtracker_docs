Servicetracker for Students
===========================

Student home page
-----------------

Student can enter their service details and also glance service projects here. At the end of year, students can enter their final reflection and print out for submission.

Input items

* service name
* blog url
* points
* staff or non-staff
* service supervisor name
* data

.. figure:: _static/servicetracker/students/student_home.png
    :align: center 
    :width: 400px


View own records
----------------

.. figure:: _static/servicetracker/students/student_record.png
    :align: center 
    :width: 400px


Goal
----
Students are able to add their goal at the beginning of the academic year.

.. figure:: _static/students/student-goal.png
    :align: center 
    :width: 400px

 
Reflection
----------
Students are able to add their reflection at the end of the year.

.. figure:: _static/students/student-reflection.png
    :align: center 
    :width: 400px


Service Learning Outcomes
-------------------------
This explains MYP and CA service learning outcomes.

.. figure:: _static/students/student-outcomes.png
    :align: center 
    :width: 400px


Not able to change parent email after registration
--------------------------------------------------
To avoid bogus email. Only your advisory is able to change it.

.. figure:: _static/students/student-not-edit.png
    :align: center 
    :width: 400px


Service Archive
---------------
This shows all service records, goals and reflections.

.. figure:: _static/students/student-service-archive.png
    :align: center 
    :width: 400px


Service Records
---------------

You can view student's Service outcomes in a bar graph, Service Goal and Reflection.

.. figure:: _static/teachersl/teacher-student-chart.png
    :align: center 
    :width: 400px


Filterable Service Opportunities
--------------------------------

.. figure:: _static/teachers/teacher-student-filterable.png
    :align: center 
    :width: 400px



