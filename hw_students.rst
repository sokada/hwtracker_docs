Hwtracker for Students
======================

Quick to send email. 
--------------------
It will take 1 min to compose email and send.

.. figure:: _static/hwtracker/students/sendemail.png
    :align: center 
    :width: 400px

Summary Table
-------------
A table is easy to glance. 


.. figure:: _static/hwtracker/students/progress_history.png
    :align: center 
    :width: 400px

Charts by weekly, monthly and subjects
--------------------------------------

.. figure:: _static/hwtracker/students/chart_student.png
    :align: center 
    :width: 400px

Send homework completion email
-------------------------------
Student is able to send a homework completion email.

.. figure:: _static/hwtracker/students/completion.png
    :align: center 
    :width: 400px

Easy to set up Personal settings
--------------------------------
Setting items

- password
- avatar
- usename
- email
- first name
- last name
- advisor
- parent email 1
- parent email 2


.. figure:: _static/hwtracker/students/student_settings.png
    :align: center 
    :width: 400px


Subject settings
----------------
In subject settings, students can set their own subjects. Students can add more than one subject to the same period.

.. figure:: _static/hwtracker/students/subject_settings.png
    :align: center 
    :width: 400px



