Servicetracker for Teachers
===========================

Advisory Menu
-------------
Here you can control advisees' parent emails and Goal.

.. figure:: _static/teachers/teacher-menu.png
    :align: center 
    :width: 400px


Allow to write goals
--------------------
Admin will allow all students to write Service goals. But this allows Advisor to control by individual student when you have a new student.
Only advisors and admin can update parent emails to avoid bogus emails.

.. figure:: _static/teachers/teacher-allow.png
    :align: center 
    :width: 400px


Add Service Opportunities
-------------------------

.. figure:: _static/teacher/teacher-opportunity.png
    :align: center 
    :width: 400px


Service Learning Outcomes
-------------------------

This explains MYP and CA service learning outcomes.

.. figure:: _static/teachers/teacher-student-outcomes.png
    :align: center 
    :width: 400px

Service Records
---------------

You can view student's Service outcomes in a bar graph, Service Goal and Reflection.

.. figure:: _static/teachers/teacher-student-chart.png
    :align: center 
    :width: 400px


Filterable Service Opportunities
--------------------------------

.. figure:: _static/teachers/teacher-student-filterable.png
    :align: center 
    :width: 400px







