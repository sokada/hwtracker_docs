Introduction
============

Hwtracker
---------
Better than email or gmail. The Hwtracker is made as a tool to support students organisational skills. This will allow students take more responsibility to their homework.

Servicetracker
--------------
The International Baccalaureate® (IB) Middle Years Programme (MYP) requires students service. The Service tracker is a simple tool which tracks and records student services. This module can be added to Hwtracker.

Servicetracker workflow
^^^^^^^^^^^^^^^^^^^^^^^

1. A student writes a blog post about her/his service project.
2. The student enters her/his service project to the Servicetracker. 
3. The Servicetracker email to a service supervisor in case of school staff memeber and email to a service admin in case of non-staff member.
4. The service supervisor approve or correct the student's service project.
5. At the end of each semester, the student write a reflection. 
6. At the end of the year, the student print out their service page from the Servicetracker to submit to the service admin. 

Requirement
-----------
Php5.6+

- mcrypt
- memcache
- mbstring

MySQL

Contact
-------
If you have any questions, please email to sokada(at)canacad.ac.jp


