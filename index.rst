.. Hwtracker documentation master file, created by
   sphinx-quickstart on Sat Jun  6 09:41:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hwtracker's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   hw_students
   hw_teachers
   hw_admin
   hw_parents
   ser_students
   ser_teachers
   ser_admin
   ser_parents
   updates
