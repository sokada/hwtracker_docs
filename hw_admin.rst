Hwtracker for School Admin
==========================

A site-administrator has a full control of the site. 

- Members
- Access control
  - Permissions
  - Groups
  - Resources 
- Settings



Dashboard
---------
Menu, Analytics, Site-wide Statistics, RSS feed

.. figure:: _static/hwtracker/admin/dashboard.png
    :align: center 
    :width: 400px


Check at risk students
----------------------

.. figure:: _static/hwtracker/admin/risk_student.png
    :align: center 
    :width: 400px

Easy to see individual students progress
----------------------------------------

.. figure:: _static/hwtracker/admin/student_view.png
    :align: center 
    :width: 400px


Weekly and monthly chart for individual student record
------------------------------------------------------

.. figure:: _static/hwtracker/admin/charts.png
    :align: center 
    :width: 400px


School wide statistics by grades and year
-----------------------------------------

.. figure:: _static/hwtracker/admin/stats.png
    :align: center 
    :width: 400px


Explore data by grades and dates
--------------------------------

.. figure:: _static/hwtracker/admin/data.png
    :align: center 
    :width: 400px


Subjects Page
-------------

.. figure:: _static/hwtracker/admin/subjects.png
    :align: center 
    :width: 400px


Teachers Page
-------------

.. figure:: _static/hwtracker/admin/teacher.png
    :align: center 
    :width: 400px


Flexible Settings
-----------------

.. figure:: _static/hwtracker/admin/settings.png
    :align: center 
    :width: 400px


Hwtracker Settings
------------------

.. figure:: _static/hwtracker/admin/hwsettings.png
    :align: center 
    :width: 400px


