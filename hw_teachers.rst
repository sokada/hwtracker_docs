Hwtracker for Teachers
======================

View advisory/homeroom group
----------------------------

.. figure:: _static/hwtracker/teachers/advisory.png
    :align: center 
    :width: 400px

View subject records
--------------------

.. figure:: _static/hwtracker/teachers/subject_record.png
    :align: center 
    :width: 400px

View individual student records
-------------------------------

.. figure:: _static/hwtracker/teachers/individual_student.png
    :align: center 
    :width: 400px

View school statistics by month, week, year and grade
-----------------------------------------------------

.. figure:: _static/hwtracker/teachers/teacher_stats.png
    :align: center 
    :width: 400px

Add records
-----------

.. figure:: _static/hwtracker/teachers/teacher_add.png
    :align: center 
    :width: 400px

Explore data by grades and dates
--------------------------------

.. figure:: _static/hwtracker/teachers/teacher_data_by_grade.png
    :align: center 
    :width: 400px

Easy setting
------------

.. figure:: _static/hwtracker/teachers/teacher_settings.png
    :align: center 
    :width: 400px

